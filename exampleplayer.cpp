#include "exampleplayer.h"
#include <limits>



/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    board = new Board();
    occupied = 0;
    this->side = side;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    delete board;
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * Going depth 1 with least-opponent-moves heuristic
     */ 
    if(opponentsMove != NULL) {
        board->doMove(opponentsMove, side == WHITE ? BLACK : WHITE);
    }
    if(!(board->hasMoves(side))) {
        return NULL;
    }
    Moves moves = getMoves(board,side);
    Move * best = new Move(-1,-1);
    int nummoves = std::numeric_limits<int>::max();
    for( int i = 0; i < moves.size; i++ ) {
        Board* b;
        b = board->copy();
        Move move(moves.moves[i]);
        b->doMove(&move,side);
        Moves opmoves = getMoves(b,side == WHITE ? BLACK :WHITE);
        if(opmoves.size < nummoves ) {
            best->x = moves.moves[i].x;
            best->y = moves.moves[i].y;
            nummoves = opmoves.size;
        }
    }
    board->doMove(best,side);
    return best;
}

Moves ExamplePlayer::getMoves(Board* b,Side s) {
    Moves moves;
    moves.size = 0;
    for(int i = 0; i < 8; i++) {
        for( int j = 0; j < 8; j++ ) {
            if( b->checkMove(new Move(i,j),s)) {
                Move move(i,j);
                moves.moves[moves.size] = move;
                moves.size += 1;
            }
        }    
    }
    return moves;
}

