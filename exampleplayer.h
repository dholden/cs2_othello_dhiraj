#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;
struct Moves {
    int size;
    Move moves[64];
};
class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
private:
    Board* board;
    Side side;
    Moves getMoves(Board* b, Side s);
    //occupied is a 64-bit integer where square (x,y) is occupied iff bit 8*x + y is 1
    unsigned long int occupied;
};

#endif
